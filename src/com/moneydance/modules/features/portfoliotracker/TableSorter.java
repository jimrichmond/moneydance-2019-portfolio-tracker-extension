/**
 * TableSorter is a decorator for TableModels; adding sorting functionality to a
 * supplied TableModel. TableSorter does not store or copy the data in its
 * TableModel; instead it maintains a map from the row indexes of the view to
 * the row indexes of the model. As requests are made of the sorter (like
 * getValueAt(row, col)) they are passed to the underlying model after the row
 * numbers have been translated via the internal mapping array. This way, the
 * TableSorter appears to hold another copy of the table with the rows in a
 * different order. <p/> TableSorter registers itself as a listener to the
 * underlying model, just as the JTable itself would. Events recieved from the
 * model are examined, sometimes manipulated (typically widened), and then
 * passed on to the TableSorter's listeners (typically the JTable). If a change
 * to the model has invalidated the order of TableSorter's rows, a note of this
 * is made and the sorter will resort the rows the next time a value is
 * requested. <p/> When the tableHeader property is set, either by using the
 * setTableHeader() method or the two argument constructor, the table header may
 * be used as a complete UI for TableSorter. The default renderer of the
 * tableHeader is decorated with a renderer that indicates the sorting status of
 * each column. In addition, a mouse listener is installed with the following
 * behavior:
 * <ul>
 * <li> Mouse-click: Clears the sorting status of all other columns and advances
 * the sorting status of that column through three values: {NOT_SORTED,
 * ASCENDING, DESCENDING} (then back to NOT_SORTED again).
 * <li> SHIFT-mouse-click: Clears the sorting status of all other columns and
 * cycles the sorting status of the column through the same three values, in the
 * opposite order: {NOT_SORTED, DESCENDING, ASCENDING}.
 * <li> CONTROL-mouse-click and CONTROL-SHIFT-mouse-click: as above except that
 * the changes to the column do not cancel the statuses of columns that are
 * already sorting - giving a way to initiate a compound sort.
 * </ul>
 * <p/> This class first appeared in the swing table demos in 1997 (v1.5) and
 * then had a major rewrite in 2004 (v2.0) to make it compatible with Java 1.4.
 * <p/> This rewrite makes the class compile cleanly with Java 1.5 while
 * maintaining backward compatibility with TableSorter v2.0.
 * 
 * @author Philip Milne
 * @author Brendon McLean
 * @author Dan van Enckevort
 * @author Parwinder Sekhon
 * @author ouroborus@ouroborus.org
 * @author Jim Richmond
 * @version 2.2 02-13-08
 * 
 * 02-13-08  - v2.2 minor changes by Jim Richmond
 * 1) Changed comparator to handle sort on  table strings 
 *    (attempt to convert strings to numbers but first remove $, %, and other stuff)
 * 2) Incorporated Multi-line header label capability to header renderer and
 *    changed color of header to match simulation color scheme
 * 3) Added mouse-motion listener to debounce extraneous sorting from mouse clicks
 *    for column resizing
 * 4) Added support for "blank" icon in Arrow class, which is used to pad header
 *    lines in a sorted column that don't have arrow in them 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
 
 package com.moneydance.modules.features.portfoliotracker;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.*;
import javax.swing.border.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

public class TableSorter extends AbstractTableModel {
	protected TableModel tableModel;

	public static final int DESCENDING = -1;
	public static final int NOT_SORTED = 0;
	public static final int ASCENDING = 1;

	private static Directive EMPTY_DIRECTIVE = new Directive(-1, NOT_SORTED);

	public static final Comparator<Object> COMPARABLE_COMPARATOR = new Comparator<Object>() {
		public int compare(Object o1, Object o2) {

			String s1 = "" + o1;
			String s2 = "" + o2;
			// s1 = s1.replace(oldChar, newChar)
			double d1 = customStringToDouble(s1);
			double d2 = customStringToDouble(s2);
			if ((d1 != -1) && (d2 != -1))
				return Double.compare(d1, d2);
			else
				return s1.compareTo(s2);

			/*
			 * Method m; try { // See if o1 is capable of comparing itself to o2 m =
			 * o1.getClass().getDeclaredMethod("compareTo",o2.getClass()); } catch
			 * (NoSuchMethodException e) { throw new ClassCastException(); }
			 * 
			 * Object retVal; try { // make the comparison retVal = m.invoke(o1,o2); }
			 * catch (IllegalAccessException e) { throw new ClassCastException(); }
			 * catch (InvocationTargetException e) { throw new ClassCastException(); } //
			 * Comparable.compareTo() is supposed to return int but invoke() //
			 * returns Object. We can't cast an Object to an int but we can // cast it
			 * to an Integer and then extract the int from the Integer. // But first,
			 * make sure it can be done. Integer i = new Integer(0);
			 * if(!i.getClass().isInstance(retVal)) { throw new ClassCastException(); }
			 * 
			 * return i.getClass().cast(retVal).intValue();
			 */
		}
	};

	public static final Comparator<Object> LEXICAL_COMPARATOR = new Comparator<Object>() {
		public int compare(Object o1, Object o2) {
			String s1 = "" + o1;
			String s2 = "" + o2;
			// s1 = s1.replace(oldChar, newChar)
			double d1 = customStringToDouble(s1);
			double d2 = customStringToDouble(s2);
			if ((d1 != -1) && (d2 != -1))
				return Double.compare(d1, d2);
			else
				return s1.compareTo(s2);
			// return o1.toString().compareTo(o2.toString());
		}
	};

	private Row[] viewToModel;
	private int[] modelToView;

	private JTableHeader tableHeader;
	private Color tableHeaderColor;
	private MouseHandler mouseHandler;
	private TableModelListener tableModelListener;
	private Map<Class, Comparator<Object>> columnComparators = new HashMap<Class, Comparator<Object>>();
	private List<Directive> sortingColumns = new ArrayList<Directive>();

	private int selectedRow;

	public TableSorter() {
		this.mouseHandler = new MouseHandler();
		this.tableModelListener = new TableModelHandler();
		this.selectedRow = -1;
        tableHeaderColor = Color.WHITE;
	}
	public TableSorter(TableModel tableModel) {
		this();
		setTableModel(tableModel);
	}
	public TableSorter(TableModel tableModel, JTableHeader tableHeader) {

		this();
		setTableHeader(tableHeader);
		setTableModel(tableModel);
	}
	public TableSorter(TableModel tableModel, JTableHeader tableHeader, Color _tableHeaderColor) {

		this();
		setTableHeader(tableHeader);
		setTableModel(tableModel);
        tableHeaderColor = _tableHeaderColor;
	}
	// Jim R - Custom conversion function for sorting table with numbers and percent
	// data in it
	private static double customStringToDouble(String str) {
		double retVal = 0;

		str = str.replace('$', ' ');
		if (str.indexOf('(') != -1)
			str = str.substring(0, str.indexOf('('));
		if (str.indexOf('%') != -1)
			str = str.substring(0, str.indexOf('%'));
		str = str.trim();

		while (str.indexOf(',') != -1) {
			int i = str.indexOf(',');
			str = str.substring(0, i) + str.substring(i + 1, str.length());
		}
		try {
			retVal = Double.parseDouble(str);
		} catch (Exception e) {
			retVal = -1;
		}
		return retVal;
	}

	private void clearSortingState() {
		viewToModel = null;
		modelToView = null;
	}

	public TableModel getTableModel() {
		return tableModel;
	}

	public synchronized void setTableModel(TableModel tableModel) {
		if (this.tableModel != null) {
			this.tableModel.removeTableModelListener(tableModelListener);
		}

		this.tableModel = tableModel;
		if (this.tableModel != null) {
			this.tableModel.addTableModelListener(tableModelListener);
		}

		clearSortingState();
		fireTableStructureChanged();
	}

	public JTableHeader getTableHeader() {
		return tableHeader;
	}

	public void setTableHeader(JTableHeader tableHeader) {
		if (this.tableHeader != null) {
			this.tableHeader.removeMouseListener(mouseHandler);
			this.tableHeader.removeMouseMotionListener(mouseHandler);
			TableCellRenderer defaultRenderer = this.tableHeader.getDefaultRenderer();
			if (defaultRenderer instanceof SortableHeaderRenderer) {
				this.tableHeader
						.setDefaultRenderer(((SortableHeaderRenderer) defaultRenderer).tableCellRenderer);
			}
		}
		this.tableHeader = tableHeader;
		if (this.tableHeader != null) {
			this.tableHeader.addMouseListener(mouseHandler);
			this.tableHeader.addMouseMotionListener((MouseMotionListener) mouseHandler);
			this.tableHeader.setDefaultRenderer(new SortableHeaderRenderer(
					this.tableHeader.getDefaultRenderer()));
		}
	}

	public boolean isSorting() {
		return sortingColumns.size() != 0;
	}

	private Directive getDirective(int column) {
		for (int i = 0; i < sortingColumns.size(); i++) {
			Directive directive = sortingColumns.get(i);
			if (directive.column == column) {
				return directive;
			}
		}
		return EMPTY_DIRECTIVE;
	}

	public int getSortingStatus(int column) {
		return getDirective(column).direction;
	}

	private void sortingStatusChanged() {
		clearSortingState();
		fireTableDataChanged();
		if (tableHeader != null) {
			JTable table = tableHeader.getTable();
			if ((table != null) && (selectedRow >= 0) && (selectedRow < table.getRowCount())) {
				table.changeSelection(viewIndex(selectedRow), 0, false, false);
			}
			tableHeader.repaint();
		}
	}

	public void setSortingStatus(int column, int status) {
		Directive directive = getDirective(column);
		if (directive != EMPTY_DIRECTIVE) {
			sortingColumns.remove(directive);
		}
		if (status != NOT_SORTED) {
			sortingColumns.add(new Directive(column, status));
		}
		sortingStatusChanged();
	}

	protected Icon getHeaderRendererIcon(int column, int size) {
		Directive directive = getDirective(column);
		if (directive == EMPTY_DIRECTIVE) {
			return null;
		}
		return new Arrow(directive.direction == DESCENDING, size, sortingColumns
				.indexOf(directive));
	}

	private void cancelSorting() {
		sortingColumns.clear();
		sortingStatusChanged();
	}

	public void setColumnComparator(Class type, Comparator<Object> comparator) {
		if (comparator == null) {
			columnComparators.remove(type);
		} else {
			columnComparators.put(type, comparator);
		}
	}

	protected Comparator<Object> getComparator(int column) {
		Class columnType = tableModel.getColumnClass(column);
		Comparator<Object> comparator = columnComparators.get(columnType);
		if (comparator != null) {
			return comparator;
		}
		if (Comparable.class.isAssignableFrom(columnType)) {
			return COMPARABLE_COMPARATOR;
		}
		return LEXICAL_COMPARATOR;
	}

	private synchronized Row[] getViewToModel() {
		if ((viewToModel != null) && (tableModel != null)
				&& (tableModel.getRowCount() != viewToModel.length)) {
			clearSortingState();
		}

		if (viewToModel == null) {
			int tableModelRowCount = tableModel.getRowCount();
			viewToModel = new Row[tableModelRowCount];
			for (int row = 0; row < tableModelRowCount; row++) {
				viewToModel[row] = new Row(row);
			}

			if (isSorting()) {
				Arrays.sort(viewToModel);
			}
		}
		return viewToModel;
	}

	public synchronized int modelIndex(int viewIndex) {
		if ((viewToModel != null) && (tableModel != null)
				&& (tableModel.getRowCount() != viewToModel.length)) {
			clearSortingState();
		}
		if ((viewIndex != -1) && (viewIndex < tableModel.getRowCount()))
			return getViewToModel()[viewIndex].modelIndex;
		else
			return -1;

	}

	private synchronized int[] getModelToView() {
		if ((modelToView != null) && (tableModel != null)
				&& (tableModel.getRowCount() != modelToView.length)) {
			clearSortingState();
		}
		if (modelToView == null) {
			int n = getViewToModel().length;
			modelToView = new int[n];
			for (int i = 0; i < n; i++) {
				modelToView[modelIndex(i)] = i;
			}
		}
		return modelToView;
	}

	public synchronized int viewIndex(int modelIndex) {
		if ((modelIndex != -1) && (modelIndex < tableModel.getRowCount()))
			return getModelToView()[modelIndex];
		else
			return -1;

	}

	// TableModel interface methods

	public int getRowCount() {
		return (tableModel == null) ? 0 : tableModel.getRowCount();
	}

	public int getColumnCount() {
		return (tableModel == null) ? 0 : tableModel.getColumnCount();
	}

	public String getColumnName(int column) {
		return tableModel.getColumnName(column);
	}

	public Class getColumnClass(int column) {
		return tableModel.getColumnClass(column);
	}

	public boolean isCellEditable(int row, int column) {
		return tableModel.isCellEditable(modelIndex(row), column);
	}

	// Jim R - rewrote to catch the null pointer exception generated if table
	// is changed at wrong time
	public Object getValueAt(int row, int column) 
  {
  	try
  	{
  		return tableModel.getValueAt(modelIndex(row), column);
  	}
  	catch (java.lang.NullPointerException e) {
  		return null;
  	}
  	catch (java.lang.ArrayIndexOutOfBoundsException e) {
  		return null;
  	}
  }

	/*public Object getValueAt(int row, int column) {
		return tableModel.getValueAt(modelIndex(row), column);
	}*/

	public void setValueAt(Object aValue, int row, int column) {
		tableModel.setValueAt(aValue, modelIndex(row), column);
	}

	// Helper classes

	private class Row implements Comparable<Row> {
		private int modelIndex;

		public Row(int index) {
			this.modelIndex = index;
		}

		public int compareTo(Row o) {
			int row1 = modelIndex;
			int row2 = o.modelIndex;

			for (Iterator<Directive> it = sortingColumns.iterator(); it.hasNext();) {
				Directive directive = it.next();
				int column = directive.column;

				Object o1 = tableModel.getValueAt(row1, column);
				Object o2 = tableModel.getValueAt(row2, column);

				int comparison = 0;
				// Define null less than everything, except null.
				if (o1 == null && o2 == null) {
					comparison = 0;
				} else if (o1 == null) {
					comparison = -1;
				} else if (o2 == null) {
					comparison = 1;
				} else {
					comparison = getComparator(column).compare(o1, o2);
				}
				if (comparison != 0) {
					return directive.direction == DESCENDING ? -comparison : comparison;
				}
			}
			return 0;
		}
	}

	private class TableModelHandler implements TableModelListener {
		public synchronized void tableChanged(TableModelEvent e) {
			// If we're not sorting by anything, just pass the event along.
			if (!isSorting()) {
				clearSortingState();
				fireTableChanged(e);
				return;
			}

			// If the table structure has changed, cancel the sorting; the
			// sorting columns may have been either moved or deleted from
			// the model.
			if (e.getFirstRow() == TableModelEvent.HEADER_ROW) {
				cancelSorting();
				fireTableChanged(e);
				return;
			}

			// We can map a cell event through to the view without widening
			// when the following conditions apply:
			//
			// a) all the changes are on one row (e.getFirstRow() == e.getLastRow())
			// and,
			// b) all the changes are in one column (column !=
			// TableModelEvent.ALL_COLUMNS) and,
			// c) we are not sorting on that column (getSortingStatus(column) ==
			// NOT_SORTED) and,
			// d) a reverse lookup will not trigger a sort (modelToView != null)
			//
			// Note: INSERT and DELETE events fail this test as they have column ==
			// ALL_COLUMNS.
			//
			// The last check, for (modelToView != null) is to see if modelToView
			// is already allocated. If we don't do this check; sorting can become
			// a performance bottleneck for applications where cells
			// change rapidly in different parts of the table. If cells
			// change alternately in the sorting column and then outside of
			// it this class can end up re-sorting on alternate cell updates -
			// which can be a performance problem for large tables. The last
			// clause avoids this problem.
			int column = e.getColumn();
			if ((e.getFirstRow() == e.getLastRow())
			   && (column != TableModelEvent.ALL_COLUMNS)
					&& (getSortingStatus(column) == NOT_SORTED) && (modelToView != null)) {

				int viewIndex = getModelToView()[e.getFirstRow()];
				fireTableChanged(new TableModelEvent(TableSorter.this, viewIndex,
						viewIndex, column, e.getType()));
				return;
			} else if ((column == TableModelEvent.ALL_COLUMNS) &&
			          (e.getType() != TableModelEvent.DELETE)){
				sortingStatusChanged();
				return;
			}

			// Something has happened to the data that may have invalidated the row
			// order.
			clearSortingState();
			fireTableDataChanged();
			return;
		}
	}

	public class MouseHandler implements MouseListener, MouseMotionListener {
		
		boolean gotPressedEvent;
		
		public MouseHandler() {
		  gotPressedEvent=false; // Keep track of state - looking for pressed then released with no 
		                         // motion
		}
		public void mouseExited(MouseEvent e) {	}
		public void mouseEntered(MouseEvent e) {}
		public void mouseClicked(MouseEvent e) {}
		public void mouseMoved(MouseEvent e) {}
		public void mouseDragged(MouseEvent e) {
      gotPressedEvent = false;
		}
		public void mousePressed(MouseEvent e) {
			gotPressedEvent = true;
		}
		public void mouseReleased(MouseEvent e) {
			
      if (javax.swing.SwingUtilities.isRightMouseButton(e)) { // ignore (right-click) clicks
      	gotPressedEvent = false;
         return;
      }
      else if (gotPressedEvent == false) 
      	return;
      	
      gotPressedEvent = false;
      if (e.getSource() instanceof JTableHeader) {
			JTableHeader h = (JTableHeader) e.getSource();
			TableColumnModel columnModel = h.getColumnModel();
			int viewColumn = columnModel.getColumnIndexAtX(e.getX());
			int column = columnModel.getColumn(viewColumn).getModelIndex();
			if (column != -1) {
				int status = getSortingStatus(column);
				if (h.getTable().getSelectedRowCount() > 0) {
					selectedRow = modelIndex(h.getTable().getSelectedRow());
				}
				if (!e.isControlDown()) {
					cancelSorting();
				}
				// Cycle the sorting states through {NOT_SORTED, ASCENDING, DESCENDING}
				// or
				// {NOT_SORTED, DESCENDING, ASCENDING} depending on whether shift is
				// pressed.
				status = status + (e.isShiftDown() ? -1 : 1);
				status = (status + 4) % 3 - 1; // signed mod, returning {-1, 0, 1}
				setSortingStatus(column, status);
			}
      }
		}
	}

	private static class Arrow implements Icon {
		private boolean descending;
		private int size;
		private int priority;
		private boolean hidden;

		public Arrow(boolean descending, int size, int priority) {
			this.descending = descending;
			this.size = size;
			this.priority = priority;
			this.hidden = false;
		}

		public Arrow(int size) { // create blank icon to take up space
			this.size = size;
			this.hidden = true;
		}

		public void paintIcon(Component c, Graphics g, int x, int y) {

			if (hidden == true)
				return;
			Color color = c == null ? Color.GRAY : c.getBackground();
			// In a compound sort, make each succesive triangle 20%
			// smaller than the previous one.
			int dx = (int) (size / 2 * Math.pow(0.8, priority));
			int dy = descending ? dx : -dx;
			// Align icon (roughly) with font baseline.
			y = y + 5 * size / 6 + (descending ? -dy : 0);
			int shift = descending ? 1 : -1;
			g.translate(x, y);

			// Right diagonal.
			g.setColor(color.darker());
			g.drawLine(dx / 2, dy, 0, 0);
			g.drawLine(dx / 2, dy + shift, 0, shift);

			// Left diagonal.
			g.setColor(color.brighter());
			g.drawLine(dx / 2, dy, dx, 0);
			g.drawLine(dx / 2, dy + shift, dx, shift);

			// Horizontal line.
			if (descending) {
				g.setColor(color.darker().darker());
			} else {
				g.setColor(color.brighter().brighter());
			}
			g.drawLine(dx, 0, 0, 0);

			g.setColor(color);
			g.translate(-x, -y);
		}

		public int getIconWidth() {
			return size;
		}

		public int getIconHeight() {
			return size;
		}
	}

	private class SortableHeaderRenderer extends JPanel implements
			TableCellRenderer {
		private TableCellRenderer tableCellRenderer;

		public SortableHeaderRenderer(TableCellRenderer tableCellRenderer) {
			this.tableCellRenderer = tableCellRenderer;
			setOpaque(true);
			setForeground(UIManager.getColor("TableHeader.foreground"));
			setBackground(tableHeaderColor);
			setBorder(UIManager.getBorder("TableHeader.cellBorder"));
			// ListCellRenderer renderer = getCellRenderer();

			// ( (JLabel) renderer).setHorizontalAlignment(JLabel.CENTER);
			// setCellRenderer(renderer);
		}

		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) {
			/*
			 * Component c = tableCellRenderer.getTableCellRendererComponent(table,
			 * value, isSelected, hasFocus, row, column); if (c instanceof JLabel) {
			 * JLabel l = (JLabel) c; l.setHorizontalTextPosition(JLabel.LEFT); int
			 * modelColumn = table.convertColumnIndexToModel(column);
			 * l.setIcon(getHeaderRendererIcon(modelColumn, l.getFont().getSize())); }
			 * return c; }
			 */
			setFont(table.getFont());
			String str = (value == null) ? "" : value.toString();
			BufferedReader br = new BufferedReader(new StringReader(str));
			String line;
			JPanel panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			panel.setBackground(tableHeaderColor);
			panel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			panel.setOpaque(true);

			JLabel label = new JLabel("");
			int modelColumn = table.convertColumnIndexToModel(column);
			Icon arrowIcon = getHeaderRendererIcon(modelColumn, label.getFont()
					.getSize());

			try {
				while ((line = br.readLine()) != null) {
					label = new JLabel(line);
					label.setFont(table.getFont());
					label.setHorizontalAlignment(JLabel.CENTER);
					label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
					// label.setBorder(BorderFactory.createLineBorder(Color.BLACK));

					if (arrowIcon != null) {
						label.setIconTextGap(0);
						label.setVerticalTextPosition(JLabel.BOTTOM);
						label.setHorizontalTextPosition(JLabel.LEFT);
						label.setIcon(new Arrow(label.getFont().getSize())); // create blank filler arrow
					}
					panel.add(label);
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
      if (arrowIcon != null)
      	label.setIcon(arrowIcon);
			return panel;
		}
	}

	private static class Directive {
		private int column;
		private int direction;

		public Directive(int column, int direction) {
			this.column = column;
			this.direction = direction;
		}
	}
}
