/***************************************************************************\
 *  * PortfolioTrackerWindow.java
 * Copyright (c) 2019, Jim Richmond
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
\***************************************************************************/

package com.moneydance.modules.features.portfoliotracker;

import com.infinitekind.moneydance.model.*;
import com.infinitekind.util.CustomDateFormat;
import com.infinitekind.util.DateUtil;
import com.moneydance.apps.md.controller.Util;
import com.moneydance.awt.*;
import com.moneydance.awt.GridC;
import com.moneydance.awt.JDateField;

import org.apache.commons.lang3.*;

import java.io.*;
import java.util.*;
import java.text.*; 
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.TimeUnit;

public class PortfolioTrackerWindow  extends JFrame {
	private Main extension;
	private AccountBook book;
	private JDateField startDateField;
	private JCheckBox skipInactiveCB;
	private JCheckBox combineDuplicateSecuritiesCB;
	private JProgressBar progressBar;
	private JLabel progressText;
	
	private DefaultTableModel tableModel;
	private TableSorter sorter;
	private JTable table;
	private Color tableRowColor = new Color(245,252,254);
	private static final String [] tableColumnNames =  
	{" \nSecurity", " \nAccount",       "Allocation\nPercent", " \nBalance",     
		"Day\nChange",  "Prev Day\nChange", "Week\nChange",        "30 Day\nChange", 
		"YTD\nChange",  "1 Year\nChange",   "3 Year\nChange",      "5 Year\nChange" };  
	private JPopupMenu popupMenu;
	
	private JCheckBox showAccountColumnCB;
	private int accountColumnSavedWidth;
	private int accountColumnSavedMaxWidth;
	private JCheckBox hideZeroBalanceCB;
	private JCheckBox hideNoTickerCB;
	
	private JRadioButton amountRadio;
	private JRadioButton totalReturnRadio;
	private JRadioButton annualReturnRadio;
	private static int SHOW_CHANGE_AS_AMOUNT = 0;
	private static int SHOW_CHANGE_AS_TOTAL_RETURN = 1;
	private static int SHOW_CHANGE_AS_ANNUAL_RETURN = 2;
	
	private JPanel topButtonPanel;
	private JButton refreshButton;
	private JButton copyButton;
	private JButton exportButton;
	private JButton closeButton;
	
	// Define indeces for columns of the main output table
	final static int NUM_CHANGE_COLUMNS_SUPPORTED = 8;
	final static int SECURITY_COLUMN_INDEX = 0;
	final static int ACCOUNT_COLUMN_INDEX = 1;
	final static int ALLOCATION_PERCENT__COLUMN_INDEX = 2;
	final static int CURRENT_BALANCE__COLUMN_INDEX = 3;
	final static int FIRST_CHANGE_COLUMN_INDEX = 4;
	final static int FIRST_IRR_COLUMN_INDEX = 8;
	
	// The values below represent indeces that map to the array of 8 change objects in
	// the SecurityInfo object below. They also map to the change columns in the output table.
	// Each change column in the output table shows investment results over a specific time interval. 
	// The "Day Change" column is represented by index 0 and the "5 Year" column by index 7
	// These are used to index into an array of SecurityChangeInfo objects (see below) 
	final static int CH1DAY=0;
	final static int CH2DAY=1;
	final static int CHWEEK=2;
	final static int CH30DAY=3;
	final static int CHYTD=4;
	final static int CH1YR=5;
	final static int CH3YR=6;
	final static int CH5YR=7;

	// SecurityChangeInfo contains input data and results for tracking
	// performance for a security over a specific time interval. Each security
	// gets assigned 8 of these objects, one for each column in the output table.
	// Generally, each SecurityChangeInfo object contain info for one CELL in the table
	private class SecurityChangeInfo {
		int changePeriod;
		int startDate;
		int endDate;
		int daysInPeriod;
		double startValue;
		double endValue;
		ArrayList<ROICashFlowEntry> cashflows = new ArrayList<ROICashFlowEntry>();
		double netCashFlow;
		String netCashFlowString;
		double totalReturnROI;
		String totalReturnROIString;
		double annualizedReturnIRR;
		String annualizedReturnIRRString;

		public SecurityChangeInfo(int chPeriod, int startD, int endD, double startV) {
			changePeriod = chPeriod;
			startDate = startD;
			endDate = endD;
			daysInPeriod = DateUtil.calculateDaysBetween(startD, endD);
			startValue = startV;
			endValue = 0;
			netCashFlow = startV;
			netCashFlowString = ""; // filled in when roi is computed
			annualizedReturnIRR = Double.NaN;
			annualizedReturnIRRString = "";
			totalReturnROI = Double.NaN;
			totalReturnROIString = "";
		}
		public SecurityChangeInfo() {
			clear();
		}
		public void clear() {
			changePeriod = -1;
			startDate = 0;
			endDate = 0;
			daysInPeriod = 0;
			startValue = 0;
			endValue = 0;
			netCashFlow = 0;
			totalReturnROI = Double.NaN;
			annualizedReturnIRR = Double.NaN;
			totalReturnROIString = "";
			annualizedReturnIRRString = "";
			netCashFlowString = "";
			cashflows.clear();
		}
		// This method is needed for "aggregator" securities, eg "Hidden Securities & "Portfolio Total"
		// The magic here is that in addition to aggregating net cash flow amounts and start and end
		// values, this method also copies all the cash flow objects from the security passed in.  
		// The aggregated cash flows are needed to compute the "hidden security" and "total portfolio" ROI values.
		public void addValuesAndCopyChangeCashFlows(SecurityChangeInfo c) {
			
			// Fill in the change period & date range from security
			if (startDate == 0) {
				changePeriod = c.changePeriod;
				startDate = c.startDate;
				endDate = c.endDate;
				daysInPeriod = DateUtil.calculateDaysBetween(startDate, endDate);
			}
			
			startValue += c.startValue;
			endValue += c.endValue;
			netCashFlow += c.netCashFlow;
			if (c.cashflows != null)
			for (int cfIdx=0; cfIdx < c.cashflows.size(); cfIdx++)
			cashflows.add(c.cashflows.get(cfIdx));      
		}
		public void computeROIs() {     
			
			annualizedReturnIRR = InvestUtil.computeROI(cashflows, 0.05);       
			if (startValue != 0)
			totalReturnROI = netCashFlow/startValue;
			else if (endValue != 0)
			totalReturnROI = netCashFlow/endValue; 
			else if (daysInPeriod >31)
			totalReturnROI = annualizedReturnIRR*(daysInPeriod/365.0);
			else
			totalReturnROI = 0;
			
			if (changePeriod < CHYTD)
			annualizedReturnIRR = totalReturnROI;  
			
			annualizedReturnIRRString = getPercentString(annualizedReturnIRR, 2);
			totalReturnROIString = getPercentString(totalReturnROI, 2);
			netCashFlowString = getCurrencyString(netCashFlow);
		}
	}
	// SecurityInfo holds balance and change information for a single security 
	// Generally,  each of these objects contains everything needed for a single
	// ROW in the output table of the portfolio tracker window.
	public class SecurityInfo implements Comparable<SecurityInfo> {
		String name;
		String parentAccountName;
		boolean blankTicker;
		double balance;
		SecurityChangeInfo change[] = new SecurityChangeInfo[NUM_CHANGE_COLUMNS_SUPPORTED];
		
		public SecurityInfo(String _name) {
			name = _name;
			parentAccountName = "";
			balance = 0;   
			blankTicker=true;
		} 
		
		//  This constructor is called for hidden securities and total portfolio
		// securities.  Notice that change period dates don't get initialized here
		// but instead get set the first time addValues... (below) is called
		public SecurityInfo(String _name, String _pname, boolean allocateChangeInfo) {
			name = _name;
			parentAccountName = _pname;
			balance = 0;
			blankTicker=true;
			if (allocateChangeInfo == true) 
			for (int i=0;i<NUM_CHANGE_COLUMNS_SUPPORTED;i++)
			change[i] = new SecurityChangeInfo();
		} 
		public void addValuesAndCopyChangeCashFlows(SecurityInfo s) {
			balance +=s.balance;
			for (int chIdx=0;chIdx<NUM_CHANGE_COLUMNS_SUPPORTED;chIdx++)
			change[chIdx].addValuesAndCopyChangeCashFlows(s.change[chIdx]);
			// fyi addValues() method also sets up change period dates if needed
		}
		public void computeROIs() { 
			for (int chIdx=0;chIdx < NUM_CHANGE_COLUMNS_SUPPORTED; chIdx++)
			change[chIdx].computeROIs();
		}
		public void clear() {
			name = "";
			balance = 0;
			blankTicker=true;       
			for (int chIdx=0;chIdx<NUM_CHANGE_COLUMNS_SUPPORTED;chIdx++)
			change[chIdx].clear();
		}
		@Override public int compareTo(SecurityInfo s2) {
			return this.name.compareTo(s2.name);
		}
	}
	private ArrayList<SecurityInfo> securityList = new ArrayList<SecurityInfo>();
	private SecurityInfo cashSecurity;
	private SecurityInfo totalSecurities;
	
	private class PreferencesSnapshot {
		int startDate;
		boolean skipInactiveAccounts;
		boolean combineDuplicateSecurities;
		int displayMode;
		boolean showParentAccountNames;
		boolean hideZeroBalanceSecurities;
		boolean hideNoTickerSecurities;
		PreferencesSnapshot() {
			startDate = startDateField.getDateInt();
			skipInactiveAccounts = skipInactiveCB.isSelected();
			combineDuplicateSecurities = combineDuplicateSecuritiesCB.isSelected();
			displayMode = SHOW_CHANGE_AS_AMOUNT;
			if (totalReturnRadio.isSelected() == true)
			displayMode = SHOW_CHANGE_AS_TOTAL_RETURN;
			else if (annualReturnRadio.isSelected() == true)
			displayMode = SHOW_CHANGE_AS_ANNUAL_RETURN;
			showParentAccountNames = showAccountColumnCB.isSelected();
			hideZeroBalanceSecurities = hideZeroBalanceCB.isSelected();
			hideNoTickerSecurities = hideNoTickerCB.isSelected();
		}
		public PreferencesSnapshot updateTableLoadPrefsOnly() {
			PreferencesSnapshot prefs = new PreferencesSnapshot();
			prefs.startDate = this.startDate;
			prefs.skipInactiveAccounts = this.skipInactiveAccounts;
			prefs.combineDuplicateSecurities = this.combineDuplicateSecurities;
			return prefs;
		}
	}
	
	private class ReportGenThread extends Thread {
		boolean recalcNeeded = false;
		boolean tableReloadNeeded = false;
		boolean terminate = false;
		PreferencesSnapshot prefsSnapshot;
		ReportGenThread () {} 
		@Override public void run() {
			prefsSnapshot = new PreferencesSnapshot(); // shouldn't be needed but initialize just in case
			while (terminate == false) {
				while ((recalcNeeded == false) && (tableReloadNeeded == false)) {
					try { this.sleep(100); } catch (InterruptedException e) {  e.printStackTrace(); }
					if (terminate == true)
					return;
				}
				if (recalcNeeded == true) {
					prefsSnapshot = new PreferencesSnapshot();
					recalcNeeded = false;
					topButtonPanel.setVisible(false);
					progressBar.setValue(0);
					//progressText.setText("Calculating...");
					progressBar.setVisible(true);
					progressText.setVisible(true);
					generatePortfolioReport(book, prefsSnapshot);
					progressBar.setVisible(false);
					progressText.setVisible(false);
					topButtonPanel.setVisible(true);
					topButtonPanel.repaint();
				}
				else if (tableReloadNeeded == true) {
					prefsSnapshot = prefsSnapshot.updateTableLoadPrefsOnly();
					tableReloadNeeded = false;
					loadTableFromSavedData(prefsSnapshot);
				}
			}
		}
	}
	ReportGenThread reportGenThread;
	
	/*  private class RoiCalcThread extends Thread {
		int changeIndex;
		boolean calculationDone = false;
		RoiCalcThread (int _changeIndex) { changeIndex = _changeIndex; calculationDone = false; } 
		@Override public void run() {
		for (int i=0; i<(securityList.size()); i++) 
				securityList.get(i).change[changeIndex].computeROIs(); 
		totalSecurities.change[changeIndex].computeROIs();        
		calculationDone = true; 
		}
	} */
	
	public PortfolioTrackerWindow(Main extension) {
		super("Moneydance Portfolio Tracker Extension - "+ extension.getUnprotectedContext().getCurrentAccountBook().getName());
		this.extension = extension;        
		book = extension.getUnprotectedContext().getCurrentAccountBook();
		
		reportGenThread = new ReportGenThread();

		startDateField = new JDateField(extension.cdate);
		startDateField.addPropertyChangeListener(new PropertyChangeListener () {
			@Override public void propertyChange(PropertyChangeEvent arg0) {
				if (arg0.getPropertyName() == JDateField.PROP_DATE_CHANGED){
					reportGenThread.recalcNeeded = true;
				}
			}		
		});
		JPanel datePanel = new JPanel();
		datePanel.add(new JLabel("Show portfolio changes as of: "));
		datePanel.add(startDateField);
		datePanel.add(new JLabel("     "));
		
		skipInactiveCB = new JCheckBox("Skip securities in inactive accounts");
		String inactiveToolTip = "If selected, amounts for securities contained in inactive accounts will be aggregated into \"Hidden Securities\".";
		skipInactiveCB.setToolTipText(inactiveToolTip);
		skipInactiveCB.setSelected(Main.preferences.getBoolean("PortfolioTracker.SkipInactiveAccounts", true));
		skipInactiveCB.setFont(skipInactiveCB.getFont().deriveFont(Font.PLAIN));
		skipInactiveCB.setFocusable(false);
		skipInactiveCB.setBorder(null); 
		skipInactiveCB.addActionListener(new ActionListener () {
			@Override public void actionPerformed(ActionEvent e) { reportGenThread.recalcNeeded = true;  }});
		
		combineDuplicateSecuritiesCB = new JCheckBox("Combine securities from multiple accounts");
		String combineDupsToolTip = "If selected, duplicate securities in multiple accounts will be combined into one record";
		combineDuplicateSecuritiesCB.setToolTipText(combineDupsToolTip);
		combineDuplicateSecuritiesCB.setSelected(Main.preferences.getBoolean("PortfolioTracker.CombineDuplicateSecurities", false));
		combineDuplicateSecuritiesCB.setFont(combineDuplicateSecuritiesCB.getFont().deriveFont(Font.PLAIN));
		combineDuplicateSecuritiesCB.setFocusable(false);
		combineDuplicateSecuritiesCB.setBorder(null); 
		combineDuplicateSecuritiesCB.addActionListener(new ActionListener () {
			@Override public void actionPerformed(ActionEvent e) { reportGenThread.recalcNeeded = true;  }});
		
		JPanel upperCBPanel = new JPanel();
		upperCBPanel.setLayout(new BoxLayout(upperCBPanel, BoxLayout.Y_AXIS));
		upperCBPanel.add(skipInactiveCB);
		upperCBPanel.add(combineDuplicateSecuritiesCB);
		
		refreshButton = new JButton("Refresh");
		refreshButton.setToolTipText("Recompute table data");
		refreshButton.setFocusable(false);
		refreshButton.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) { reportGenThread.recalcNeeded = true; }});   
		copyButton = new JButton("Copy");
		copyButton.setToolTipText("Copy table data to clipboard");
		copyButton.setFocusable(false);
		copyButton.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) { TableExporter te = new TableExporter(table, true); }});    
		exportButton = new JButton("Export");
		exportButton.setToolTipText("Save table data to a csv file");
		exportButton.setFocusable(false);
		exportButton.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) { TableExporter te = new TableExporter(table); }});      
		topButtonPanel = new JPanel();
		topButtonPanel.add(refreshButton);
		topButtonPanel.add(copyButton);
		topButtonPanel.add(exportButton);
		
		JPanel progressBarPanel = new JPanel();
		progressBarPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		int numberOfSecurities=0;
		Iterator iterator = AccountUtil.getAccountIterator(book);
		while (iterator.hasNext()) {
			Account acct = (Account) iterator.next();
			if (acct.getAccountType() ==  Account.AccountType.SECURITY) 
			numberOfSecurities++;
		}          
		progressBar = new JProgressBar(0, numberOfSecurities);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setVisible(false);
		progressText = new JLabel("Calculating...");
		progressText.setFont(progressText.getFont().deriveFont(Font.BOLD));
		progressText.setVisible(false);
		progressBarPanel.add(progressText);
		progressBarPanel.add(progressBar);
		
		JScrollPane tableScrollPane = initializeReportTable();

		showAccountColumnCB = new JCheckBox("Show account name column");
		showAccountColumnCB.setToolTipText("If deselected, the  account name column is hidden");
		showAccountColumnCB.setSelected(Main.preferences.getBoolean("PortfolioTracker.ShowAccountColumn", true));
		showAccountColumnCB.setFont(showAccountColumnCB.getFont().deriveFont(Font.PLAIN));
		showAccountColumnCB.setFocusable(false);
		showAccountColumnCB.setBorder(null);
		showAccountColumnCB.addActionListener(new ActionListener () {
			@Override public void actionPerformed(ActionEvent e) { 
				if (showAccountColumnCB.isSelected() == true) {
					table.getColumnModel().getColumn(ACCOUNT_COLUMN_INDEX).setMinWidth(10);
					table.getColumnModel().getColumn(ACCOUNT_COLUMN_INDEX).setMaxWidth(accountColumnSavedMaxWidth);
					table.getColumnModel().getColumn(ACCOUNT_COLUMN_INDEX).setPreferredWidth(accountColumnSavedWidth);               
				} else {
					accountColumnSavedWidth = table.getColumnModel().getColumn(ACCOUNT_COLUMN_INDEX).getPreferredWidth();
					table.getColumnModel().getColumn(ACCOUNT_COLUMN_INDEX).setMinWidth(0);
					table.getColumnModel().getColumn(ACCOUNT_COLUMN_INDEX).setMaxWidth(0);
				}
			}});
		
		hideNoTickerCB = new JCheckBox("Hide securities with blank ticker");
		hideNoTickerCB.setToolTipText("If selected, amounts for securities with no ticker are aggregated into \"Hidden Securities\".");
		hideNoTickerCB.setSelected(Main.preferences.getBoolean("PortfolioTracker.HideNoTickerSecurities", true));
		hideNoTickerCB.setFont(hideNoTickerCB.getFont().deriveFont(Font.PLAIN));
		hideNoTickerCB.setFocusable(false);
		hideNoTickerCB.setBorder(null);    
		hideNoTickerCB.addActionListener(new ActionListener () {
			@Override public void actionPerformed(ActionEvent e) { reportGenThread.tableReloadNeeded = true; }});

		
		hideZeroBalanceCB = new JCheckBox("Hide securities with zero balance");
		hideZeroBalanceCB.setToolTipText("If selected,  amounts for securities with zero current balance are aggregated into \"Hidden Securities\".");
		hideZeroBalanceCB.setSelected(Main.preferences.getBoolean("PortfolioTracker.HideZeroBalanceSecurities", true));
		hideZeroBalanceCB.setFont(hideZeroBalanceCB.getFont().deriveFont(Font.PLAIN));
		hideZeroBalanceCB.setFocusable(false);
		hideZeroBalanceCB.setBorder(null);    
		hideZeroBalanceCB.addActionListener(new ActionListener () {
			@Override public void actionPerformed(ActionEvent e) { reportGenThread.tableReloadNeeded = true; }});

		JPanel bottomCBPanel2 = new JPanel();
		bottomCBPanel2.setLayout(new BoxLayout(bottomCBPanel2, BoxLayout.Y_AXIS));
		bottomCBPanel2.add(hideNoTickerCB);
		bottomCBPanel2.add(hideZeroBalanceCB);
		
		amountRadio = new JRadioButton("Amount");
		amountRadio.setBackground(Color.WHITE);
		amountRadio.setFocusable(false);
		amountRadio.setMargin(new Insets(0,0,0,7));
		String amountRadioTip = "Show the total investment gain over the period, including realized and unrealized gains and income distributions";
		amountRadio.setToolTipText(amountRadioTip);
		//   amountRadio.setBorder(null);    
		amountRadio.setSelected(Main.preferences.getBoolean("PortfolioTracker.AmountRadio", true));
		amountRadio.setFont(amountRadio.getFont().deriveFont(Font.PLAIN));
		amountRadio.addItemListener( new ItemListener () {
			@Override public void itemStateChanged(ItemEvent e) {
				if (amountRadio.isSelected() == true)
				reportGenThread.tableReloadNeeded = true;
			}});
		totalReturnRadio = new JRadioButton("Total Return (ROI)");
		totalReturnRadio.setBackground(Color.WHITE);
		totalReturnRadio.setFocusable(false);
		totalReturnRadio.setMargin(new Insets(0,0,0,7));
		//   totalReturnRadio.setBorder(null);    
		String totalReturnRadioTip = "Show total return on investment using the formula: (TotalGain/StartValue)";
		totalReturnRadio.setToolTipText(totalReturnRadioTip);
		totalReturnRadio.setSelected(Main.preferences.getBoolean("PortfolioTracker.TotalReturnRadio", false));
		totalReturnRadio.setFont(totalReturnRadio.getFont().deriveFont(Font.PLAIN));
		totalReturnRadio.addItemListener( new ItemListener () {
			@Override public void itemStateChanged(ItemEvent e) {
				if (totalReturnRadio.isSelected() == true)
				reportGenThread.tableReloadNeeded = true;
			}});
		annualReturnRadio = new JRadioButton("ROI & Average Return (IRR)");
		annualReturnRadio.setBackground(Color.WHITE);
		annualReturnRadio.setFocusable(false);
		annualReturnRadio.setMargin(new Insets(0,0,0,0));    
		//  annualReturnRadio.setBorder(null);    
		annualReturnRadio.setSelected(Main.preferences.getBoolean("PortfolioTracker.AnnualReturnRadio", false));
		annualReturnRadio.setFont(annualReturnRadio.getFont().deriveFont(Font.PLAIN));
		String annualReturnRadioTip = "Show ROI for periods < YTD, average\n annualized return or internal rate of return for last 4 columns)";
		annualReturnRadio.setToolTipText(annualReturnRadioTip);
		annualReturnRadio.addItemListener( new ItemListener () {
			@Override public void itemStateChanged(ItemEvent e) {
				if (annualReturnRadio.isSelected() == true)
				reportGenThread.tableReloadNeeded = true;
			}});
		ButtonGroup AmountROIBG = new ButtonGroup();    
		AmountROIBG.add(amountRadio);
		AmountROIBG.add(totalReturnRadio);
		AmountROIBG.add(annualReturnRadio);
		
		JPanel radioButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEADING,0,0));
		radioButtonPanel.add(new JLabel("   "));
		radioButtonPanel.add(amountRadio);
		radioButtonPanel.add(totalReturnRadio);
		radioButtonPanel.add(annualReturnRadio);

		JLabel radioLabel = new JLabel("Show portfolio changes as:");
		JPanel radioLabelPanel = new JPanel(new FlowLayout(FlowLayout.LEADING,0,0));
		radioLabelPanel.add(radioLabel);
		JPanel radioPanel = new JPanel();
		radioPanel.setLayout(new BoxLayout(radioPanel, BoxLayout.Y_AXIS));
		radioPanel.add(radioLabelPanel);
		radioPanel.add(radioButtonPanel);
		
		closeButton = new JButton("Close");
		closeButton.setToolTipText("Close Portfolio Tracker");
		closeButton.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) {
				savePreferences();           
				extension.closeConsole();  }});  
		JPanel b = new JPanel();
		b.setLayout(new BoxLayout(b, BoxLayout.X_AXIS));
		b.setBorder(new EmptyBorder(0,0,0,0));  
		b.add(showAccountColumnCB);
		b.add(Box.createHorizontalStrut(20));
		b.add(bottomCBPanel2);
		b.add(Box.createHorizontalGlue());
		b.add(radioPanel);
		b.add(Box.createHorizontalGlue());
		b.add(closeButton);
		
		JPanel p = new JPanel(new GridBagLayout()); // x,y, wtx,wty, spanx, spany fillx, filly //  top, left, bottom, right
		p.setBorder(new EmptyBorder(10,10,10,10));
		p.add(datePanel,       AwtUtil.getConstraints(0,0,0,0,1,1,false,false, GridBagConstraints.WEST,0,0,3,0));
		p.add(upperCBPanel,    AwtUtil.getConstraints(1,0,0,0,1,1,false,false, GridBagConstraints.WEST,0,0,3,0));
		p.add(topButtonPanel,  AwtUtil.getConstraints(2,0,0,0,2,1,false,false, GridBagConstraints.EAST,0,0,2,0));
		p.add(progressBarPanel,AwtUtil.getConstraints(2,0,0,0,2,1,true,false,  GridBagConstraints.EAST,4,0,4,0));
		p.add(tableScrollPane, AwtUtil.getConstraints(0,1,1,1,4,1,true,true));
		p.add(b,               AwtUtil.getConstraints(0,2,0,0,4,1,true,false,  GridBagConstraints.CENTER,5,0,0,0));
		getContentPane().add(p);
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		enableEvents(WindowEvent.WINDOW_CLOSING);
		reportGenThread.start();
		if(book !=null) {
			reportGenThread.recalcNeeded = true;
		}
		this.pack(); 
		
		accountColumnSavedWidth = Main.preferences.getInt("PortfolioTracker.AccountColumnSavedWidth", 200);    
		if (showAccountColumnCB.isSelected() == false) {
			table.getColumnModel().getColumn(ACCOUNT_COLUMN_INDEX).setMinWidth(0);
			table.getColumnModel().getColumn(ACCOUNT_COLUMN_INDEX).setMaxWidth(0);
		} 
		setSize(Main.preferences.getInt("PortfolioTracker.FrameWidth",1200), 
		Main.preferences.getInt("PortfolioTracker.FrameHeight",500));
		AwtUtil.centerWindow(this);
	}

	public JScrollPane initializeReportTable() {
		
		tableModel = new DefaultTableModel() {
			public boolean isCellEditable(int rowIndex, int mColIndex) {
				return false;
			}};

		for (int i=0;i< tableColumnNames.length;i++)
		tableModel.addColumn(tableColumnNames[i]);

		// Magic warning -- the TableSorter has a multi-line header renderer and it
		// knows how to sort data with $, %, and other stuff in it. It defaults to a string
		// sort if it can't extract a number out of a cell.
		JTableHeader header = new JTableHeader();
		sorter = new TableSorter(tableModel, header);
		table = new JTable(sorter);
		sorter.setTableHeader(table.getTableHeader());
		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer(){
			@Override  public Component getTableCellRendererComponent(JTable table,Object value,boolean isSelected,boolean hasFocus,int row,int column) {
				Component c = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
				boolean isTotalRow=false;
				if (tableModel.getValueAt(row, SECURITY_COLUMN_INDEX) != null)
				if (tableModel.getValueAt(row, SECURITY_COLUMN_INDEX).toString().equals("Portfolio Total"))
				isTotalRow = true;
				if (value != null)
				if ((value.toString().startsWith("-")) && (value.toString().equals("- ") == false))
				c.setForeground(Color.RED);
				else            
				c.setForeground(Color.BLACK);
				if (isTotalRow == true)
				c.setFont(c.getFont().deriveFont(Font.BOLD));
				else
				c.setFont(c.getFont().deriveFont(Font.PLAIN));
				if ((column == SECURITY_COLUMN_INDEX) || (column == ACCOUNT_COLUMN_INDEX))
				((DefaultTableCellRenderer)c).setHorizontalAlignment(DefaultTableCellRenderer.LEFT);
				else
				((DefaultTableCellRenderer)c).setHorizontalAlignment(DefaultTableCellRenderer.RIGHT);
				//even index, selected or not selected
				if (row % 2 == 0)
				c.setBackground(tableRowColor); 
				else
				c.setBackground(Color.white);
				return c;
			}};
		for(int i=0;i<table.getColumnModel().getColumnCount();i++)
		  table.getColumnModel().getColumn(i).setCellRenderer(renderer);

		table.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				table.scrollRectToVisible(table.getCellRect(table.getRowCount()-1, 0, true));
			}});

		table.setShowVerticalLines(true);
		table.setShowHorizontalLines(false);
		table.setRowSelectionAllowed(true);
		table.setColumnSelectionAllowed(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setSelectionForeground(Color.black);
		table.setSelectionBackground(Color.white);
		table.getTableHeader().setReorderingAllowed(false);
		//table.setFont(new Font("Lucida Sans Oblique", Font.PLAIN, 12));
		table.setRowHeight(table.getFontMetrics(table.getFont()).getHeight()+2);
		
		accountColumnSavedMaxWidth = table.getColumnModel().getColumn(ACCOUNT_COLUMN_INDEX).getMaxWidth();
		
		int [] columnWidths = Main.preferences.getIntArray("PortfolioTracker.TableColumnWidths");
		if (columnWidths.length == table.getColumnModel().getColumnCount())
		  for(int i=0;i<table.getColumnModel().getColumnCount();i++)
		table.getColumnModel().getColumn(i).setPreferredWidth(columnWidths[i]);

		table.addMouseListener(new TablePopupHandler(table)); 
		
		JScrollPane sp = new JScrollPane(table,
		JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
		JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		sp.getViewport().setScrollMode(JViewport.BACKINGSTORE_SCROLL_MODE);
		sp.getViewport().setOpaque(true);
		sp.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED)); 
		return sp;    
	}
	
	public synchronized void clearTable() {
		int rowcount = tableModel.getRowCount();
		for (int i = 1; i <= rowcount; i++)
		tableModel.removeRow(0);
	}
	
	public void generatePortfolioReport(AccountBook book, PreferencesSnapshot prefs) {
		
		long startNanos = System.nanoTime();

		int startDate = prefs.startDate;
		
		securityList.clear();             
		clearTable();
		cashSecurity = new SecurityInfo("Total Cash");
		totalSecurities = new SecurityInfo("Portfolio Total", "", true);

		int startDateMinusOneDay = DateUtil.convertDateToInt(DateUtil.decrementDate(DateUtil.convertIntDateToLong(startDate)));
		int startDateMinus2Days = DateUtil.convertDateToInt(DateUtil.decrementDate(DateUtil.convertIntDateToLong(startDateMinusOneDay)));
		int startDateMinus7Days = startDate;
		for(int i=1;i<=7;i++)
		startDateMinus7Days = DateUtil.convertDateToInt(DateUtil.decrementDate(DateUtil.convertIntDateToLong(startDateMinus7Days)));
		int dateOneMonthEarlier = DateUtil.convertDateToInt(DateUtil.decrementMonth(DateUtil.convertIntDateToLong(startDate)));
		int dateAtFirstOfYear = DateUtil.firstDayInYear(startDate);
		int dateOneYearEarlier = DateUtil.decrementYear(startDate);
		int dateThreeYearsEarlier = dateOneYearEarlier;
		dateThreeYearsEarlier = DateUtil.decrementYear(dateThreeYearsEarlier);
		dateThreeYearsEarlier = DateUtil.decrementYear(dateThreeYearsEarlier);
		int dateFiveYearsEarlier = dateThreeYearsEarlier;
		dateFiveYearsEarlier = DateUtil.decrementYear(dateFiveYearsEarlier);
		dateFiveYearsEarlier = DateUtil.decrementYear(dateFiveYearsEarlier);

		// set up arrays with start and end dates for each period
		int[] startDates = new int[9];
		startDates[CH1DAY] = startDateMinusOneDay;   
		startDates[CH2DAY] = startDateMinus2Days;
		startDates[CHWEEK] = startDateMinus7Days;
		startDates[CH30DAY] = dateOneMonthEarlier;
		startDates[CHYTD] = dateAtFirstOfYear;
		startDates[CH1YR] = dateOneYearEarlier;
		startDates[CH3YR] = dateThreeYearsEarlier;
		startDates[CH5YR] = dateFiveYearsEarlier;
		startDates[8] = startDate;  
		int [] endDates = new int[8];
		endDates[CH1DAY] = startDate;  
		endDates[CH2DAY] = startDateMinusOneDay;  
		endDates[CHWEEK] = startDate;  
		endDates[CH30DAY] = startDate;  
		endDates[CHYTD] = startDate;  
		endDates[CH1YR] = startDate;  
		endDates[CH3YR] = startDate;  
		endDates[CH5YR] = startDate;
		
		int count = 0;
		Iterator iterator = AccountUtil.getAccountIterator(book);
		while (iterator.hasNext()) {
			Account acct = (Account) iterator.next();
			if (acct == null)
			System.err.println(String.format("PortfolioTracker error: unexpected null account encountered while generating report"));
			if (acct.getAccountType() ==  Account.AccountType.SECURITY)
			count++;
			if (acct.getAccountType() ==  Account.AccountType.INVESTMENT)
			cashSecurity.balance += acct.getBalance()/100.0;
			else if ((acct.getAccountType() ==  Account.AccountType.SECURITY) &&
					((acct.getParentAccount().getAccountIsInactive() == false) || (prefs.skipInactiveAccounts == false))) {     
				progressBar.setValue(count);

				long[] shareArray = AccountUtil.getBalancesAsOfDates(book, acct, startDates);
				double[] shares = new double[9];
				for (int i=0; i<9;i++)
				shares[i] = shareArray[i]/10000.0;

				// Set up change array and put in initial cash flow for each change period
				SecurityInfo security = new SecurityInfo(acct.getAccountName(), acct.getParentAccount().getAccountName(), true);
				for (int chIdx=0; chIdx<NUM_CHANGE_COLUMNS_SUPPORTED; chIdx++) {
					double priceAtStart = 1.0/acct.getCurrencyType().getUserRateByDateInt(startDates[chIdx]);
					double startValue = priceAtStart*shares[chIdx];
					security.change[chIdx] = new SecurityChangeInfo(chIdx, startDates[chIdx],endDates[chIdx], startValue); 
					security.change[chIdx].cashflows.add(new ROICashFlowEntry(startDates[chIdx], (long) (startValue*100), acct.getCurrencyType(), startDates[chIdx]));
				}
				
				// Get all transactions within the widest date range (eg 5yrs)
				DateRange dateRange = new DateRange(DateUtil.convertIntDateToLong(startDates[CH5YR]), DateUtil.convertIntDateToLong(endDates[CH1YR]));
				TxnSet transactions = book.getTransactionSet().getTransactions(TxnUtil.getSearch(acct, dateRange));
				transactions.sortByField(AccountUtil.DATE);
				int txnCount = transactions.getSize();
				for (int i=0;i<txnCount;i++) {
					SplitTxn txn = (SplitTxn) transactions.getTxn(i);
					if (txn.getParentTxn().getInvestTxnType() !=  InvestTxnType.DIVIDEND_REINVEST)
					for (int chIdx=0; chIdx < NUM_CHANGE_COLUMNS_SUPPORTED; chIdx++)
					// check transaction date against the start/end dates of each change period to see if it should be included
					if ((txn.getDateInt() > startDates[chIdx]) && (txn.getDateInt() <= endDates[chIdx])) {
						ROICashFlowEntry cf = new ROICashFlowEntry(txn, txn.getParentTxn().getInvestTxnType(),acct.getCurrencyType(), startDates[chIdx]);
						security.change[chIdx].cashflows.add(cf);
						security.change[chIdx].netCashFlow += cf.getValue()*100;
					}
				}
				
				// Put in ending cash flow for each change period
				for (int chIdx=0; chIdx<NUM_CHANGE_COLUMNS_SUPPORTED; chIdx++) {
					double priceAtEnd = 1.0/acct.getCurrencyType().getUserRateByDateInt(security.change[chIdx].endDate);
					double sharesAtEnd = shares[8]; // most change periods end on same day, so get shares at end date
					if (chIdx == CH2DAY)
					sharesAtEnd = shares[CH1DAY]; // the 2-day period ends a day before the end (same date as 1 day period)
					double endValue = priceAtEnd*sharesAtEnd;
					security.change[chIdx].endValue = endValue;
					security.change[chIdx].netCashFlow -= endValue;
					security.change[chIdx].cashflows.add(new ROICashFlowEntry(endDates[chIdx], (long) (-endValue*100), acct.getCurrencyType(), startDates[chIdx]));
					if (security.change[chIdx].netCashFlow != 0)
					security.change[chIdx].netCashFlow = -security.change[chIdx].netCashFlow;
					security.change[chIdx].computeROIs();
				}
				security.balance = security.change[CH1DAY].endValue;
				
				String ticker = "";
				CurrencyType currencyType = acct.getCurrencyType();
				if (currencyType != null)
				ticker =  currencyType.getTickerSymbol(); 
				security.blankTicker = ticker.equals("");
				
				totalSecurities.addValuesAndCopyChangeCashFlows(security);
				
				SecurityInfo duplicateSecurity;
				if ((prefs.combineDuplicateSecurities == true) && ((duplicateSecurity = findDuplicateSecurity(security.name)) != null)) {   
					duplicateSecurity.parentAccountName =  duplicateSecurity.parentAccountName + ", " + security.parentAccountName;
					duplicateSecurity.addValuesAndCopyChangeCashFlows(security);
					duplicateSecurity.computeROIs();
				} else      
				securityList.add(security);      
			} // else if acct... 
		} //while iterator
		
		Collections.sort(securityList);
		
		totalSecurities.balance += cashSecurity.balance;
		
		/*  RoiCalcThread roiWorker[] = new RoiCalcThread[NUM_CHANGE_COLUMNS_SUPPORTED];

	for (int i=0;i<NUM_CHANGE_COLUMNS_SUPPORTED;i++) {
		roiWorker[i] = new RoiCalcThread(i);
		roiWorker[i].start();
	}
	for (int i=0;i<NUM_CHANGE_COLUMNS_SUPPORTED;i++) {
		while(roiWorker[i].calculationDone != true) {
			try { Thread.sleep(10); } catch (InterruptedException e) {  e.printStackTrace(); }
		}
	} */
		totalSecurities.computeROIs();  
		
		loadTableFromSavedData(prefs);   
		
		//System.err.println(String.format("PortfolioTracker Report Generated in %.3f seconds",(System.nanoTime()-startNanos)/1000000000.0));
	}

	public SecurityInfo findDuplicateSecurity(String searchName) {
		
		SecurityInfo duplicate = null;
		for (int i=0; i<(securityList.size()); i++) {
			SecurityInfo security = securityList.get(i);
			if (security.name.equals(searchName) == true)
			duplicate = security;
		}
		return duplicate;
	}
	
	// Load results from saved securities data onto the JTable for display
	public void loadTableFromSavedData(PreferencesSnapshot prefs) {
		
		clearTable();

		for (int i=FIRST_IRR_COLUMN_INDEX; i< tableColumnNames.length; i++)
		if (prefs.displayMode == SHOW_CHANGE_AS_ANNUAL_RETURN)
		table.getTableHeader().getColumnModel().getColumn(i).setHeaderValue(tableColumnNames[i].replaceAll("Change", "Avg. Return"));
		else
		table.getTableHeader().getColumnModel().getColumn(i).setHeaderValue(tableColumnNames[i]);
		table.getTableHeader().repaint();
		
		SecurityInfo hiddenSecurities = new SecurityInfo("Hidden Securities", "", true);
		
		for (int i=0; i<(securityList.size()); i++) {
			SecurityInfo security = securityList.get(i);
			if (((prefs.hideNoTickerSecurities == true) && (security.blankTicker == true)) ||
					((security.balance == 0) && (prefs.hideZeroBalanceSecurities == true))) {
				hiddenSecurities.addValuesAndCopyChangeCashFlows(security);
			} else
			addSecurityToResultsTable(security, prefs);
		}
		if (hiddenSecurities.change[CH5YR].netCashFlow != 0) {
			hiddenSecurities.computeROIs();  
			addSecurityToResultsTable(hiddenSecurities, prefs);   
		}      
		addSecurityToResultsTable(cashSecurity, prefs);   
		addSecurityToResultsTable(totalSecurities, prefs);   
	}
	
	public void addSecurityToResultsTable(SecurityInfo security, PreferencesSnapshot prefs) {
		
		if (security.name.equals("Total Cash"))
		tableModel.addRow(new Object [] { "Total Cash", "", 
			getPercentString(security.balance/totalSecurities.balance,1),
			getCurrencyString(security.balance),
			"","","","","","",""});  
		else if (prefs.displayMode == SHOW_CHANGE_AS_AMOUNT)
		tableModel.addRow(new Object [] {
			security.name, 
			security.parentAccountName, 
			getPercentString(security.balance/totalSecurities.balance,1),
			getCurrencyString(security.balance), 
			security.change[CH1DAY].netCashFlowString, 
			security.change[CH2DAY].netCashFlowString, 
			security.change[CHWEEK].netCashFlowString, 
			security.change[CH30DAY].netCashFlowString, 
			security.change[CHYTD].netCashFlowString, 
			security.change[CH1YR].netCashFlowString, 
			security.change[CH3YR].netCashFlowString, 
			security.change[CH5YR].netCashFlowString});
		else if (prefs.displayMode == SHOW_CHANGE_AS_TOTAL_RETURN)
		tableModel.addRow(new Object [] {
			security.name, 
			security.parentAccountName, 
			getPercentString(security.balance/totalSecurities.balance,1),
			getCurrencyString(security.balance), 
			security.change[CH1DAY].totalReturnROIString, 
			security.change[CH2DAY].totalReturnROIString, 
			security.change[CHWEEK].totalReturnROIString, 
			security.change[CH30DAY].totalReturnROIString,
			security.change[CHYTD].totalReturnROIString,
			security.change[CH1YR].totalReturnROIString,
			security.change[CH3YR].totalReturnROIString,
			security.change[CH5YR].totalReturnROIString});
		else if (prefs.displayMode == SHOW_CHANGE_AS_ANNUAL_RETURN)
		tableModel.addRow(new Object [] {
			security.name, 
			security.parentAccountName, 
			getPercentString(security.balance/totalSecurities.balance,1),
			getCurrencyString(security.balance), 
			security.change[CH1DAY].annualizedReturnIRRString, 
			security.change[CH2DAY].annualizedReturnIRRString, 
			security.change[CHWEEK].annualizedReturnIRRString, 
			security.change[CH30DAY].annualizedReturnIRRString,
			security.change[CHYTD].annualizedReturnIRRString,
			security.change[CH1YR].annualizedReturnIRRString,
			security.change[CH3YR].annualizedReturnIRRString,
			security.change[CH5YR].annualizedReturnIRRString});
		else
		tableModel.addRow(new Object [] {security.name, security.parentAccountName, 
			getPercentString(security.balance/totalSecurities.balance,1),
			getCurrencyString(security.balance),
			"","","","","","",""});
	} 
	
	public String getPercentString(double val, int numDecimalPlaces){
		NumberFormat pf = NumberFormat.getPercentInstance();
		pf.setMinimumFractionDigits(numDecimalPlaces);
		pf.setMaximumFractionDigits(numDecimalPlaces);
		if (val == 0)
		return "- ";
		else 
		return pf.format(val);
	}
	
	public String getCurrencyString(double val) {
		NumberFormat cf = NumberFormat.getCurrencyInstance();
		cf.setMinimumFractionDigits(0);
		cf.setMaximumFractionDigits(0);
		// if (val == 0)
		//     return "- ";
		// else 
		return cf.format(val);
	} 
	
	public final void processEvent(AWTEvent evt) {
		if(evt.getID()==WindowEvent.WINDOW_CLOSING) {
			savePreferences();
			reportGenThread.terminate = true;
			extension.closeConsole();
			return;
		}
		if(evt.getID()==WindowEvent.WINDOW_OPENED) {
		}
		super.processEvent(evt);
	}
	
	private void savePreferences() {
		if (showAccountColumnCB.isSelected() == true)
		accountColumnSavedWidth = table.getColumnModel().getColumn(1).getPreferredWidth();

		Dimension frameDimension = this.getSize();
		Main.preferences.put("PortfolioTracker.FrameWidth", frameDimension.width);
		Main.preferences.put("PortfolioTracker.FrameHeight", frameDimension.height);
		Main.preferences.put("PortfolioTracker.SkipInactiveAccounts", skipInactiveCB.isSelected());
		Main.preferences.put("PortfolioTracker.CombineDuplicateSecurities", combineDuplicateSecuritiesCB.isSelected());
		Main.preferences.put("PortfolioTracker.ShowAccountColumn", showAccountColumnCB.isSelected());
		Main.preferences.put("PortfolioTracker.AccountColumnSavedWidth", accountColumnSavedWidth);
		Main.preferences.put("PortfolioTracker.HideNoTickerSecurities", hideNoTickerCB.isSelected());
		Main.preferences.put("PortfolioTracker.HideZeroBalanceSecurities", hideZeroBalanceCB.isSelected());
		Main.preferences.put("PortfolioTracker.AmountRadio", amountRadio.isSelected());
		Main.preferences.put("PortfolioTracker.TotalReturnRadio", totalReturnRadio.isSelected());
		Main.preferences.put("PortfolioTracker.AnnualReturnRadio", annualReturnRadio.isSelected());

		int [] columnWidths = new int[table.getColumnModel().getColumnCount()]; 
		for(int i=0;i<table.getColumnModel().getColumnCount();i++)
		columnWidths[i] = table.getColumnModel().getColumn(i).getPreferredWidth();
		Main.preferences.put("PortfolioTracker.TableColumnWidths", columnWidths);
		
		Main.preferences.isDirty();
	}
	
	class TablePopupHandler extends MouseAdapter implements ActionListener {

		JTable table;
		JPopupMenu popupMenu;
		
		private TablePopupHandler(JTable _table) {

			table = _table;
			
			popupMenu = new JPopupMenu();
			popupMenu.add(createPopupMenuItem("Copy Table To Clipboard"));
			popupMenu.add(createPopupMenuItem("Export Table Data..."));
		}
		private JMenuItem createPopupMenuItem(String text) {
			JMenuItem menuItem= new JMenuItem(text);
			menuItem.setFont(menuItem.getFont().deriveFont(12f).deriveFont(Font.PLAIN));
			menuItem.addActionListener(this);
			return menuItem;
		}

		public void mouseReleased(MouseEvent e) {
			if (javax.swing.SwingUtilities.isRightMouseButton(e)) 
			popupMenu.show(e.getComponent(), e.getX(), e.getY());
		}

		public void actionPerformed(ActionEvent e) {

			String op = e.getActionCommand();
			if (op.equals("Export Table Data...")) {
				TableExporter te = new TableExporter(table);
			}  else if (op.equals("Copy Table To Clipboard")) {
				TableExporter te = new TableExporter(table, true);
			}
		}
	}

	void goAway() {
		savePreferences();
		setVisible(false);
		reportGenThread.terminate = true;
		dispose();
	}
}
