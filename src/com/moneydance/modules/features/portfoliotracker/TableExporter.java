package com.moneydance.modules.features.portfoliotracker;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.TableColumnModel;

public class TableExporter {

	public TableExporter(JTable table) {
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
		chooser.setFileFilter(new FileFilter() {
			public boolean accept(File f) {
				return f.getName().toLowerCase().endsWith(".csv") || f.isDirectory();
			}
			public String getDescription() {
				return "CSV Files";
			}
		});

		int state = chooser.showSaveDialog(null);
		if (state == JFileChooser.APPROVE_OPTION) {
			String currentDatafile = chooser.getSelectedFile().getPath();
			if (!currentDatafile.toLowerCase().endsWith(".csv")) {
				currentDatafile += ".csv";
			}
			File file = new File(currentDatafile);
			int userResponse = JOptionPane.OK_OPTION;
			if (file.exists()) {
				userResponse = JOptionPane.showConfirmDialog(null, "The file \""
						+ file.getName() + "\" already exists.\nDo you want to replace it?",
						"Confirm Save", JOptionPane.YES_NO_OPTION,
						JOptionPane.WARNING_MESSAGE);
			}
			if (userResponse == JOptionPane.OK_OPTION) {

				try {
					BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
							file, false));
					PrintWriter fileWriter = new PrintWriter(bufferedWriter);

					TableColumnModel cm = table.getColumnModel();
					for (int i=0;i<cm.getColumnCount(); i++) {
						String s = ""+cm.getColumn(i).getHeaderValue();
            s = s.replace('\n',' ');
            s = s.trim();
            if (i>0)
            	s = ", " + s;
						fileWriter.print(s);					
					}
					fileWriter.print("\n");
					
					for (int i = 0; i < table.getRowCount(); ++i) {
						for (int j = 0; j < table.getColumnCount(); ++j) {
						  String s;
						  if (table.getValueAt(i,j) != null)
						  	s = table.getValueAt(i, j).toString();
						  else
						   	s = "";
							
							// clean up the text string to remove $, %, and commas
	            s = s.replaceAll("\\$", "");
	            s = s.replaceAll("%", "");
	            s = s.replaceAll(", ", "; ");
	            s = s.replaceAll(",", "");
	            if (j > 0)
	              	s = ", " + s;
							fileWriter.print(s);
						}
						fileWriter.println("");
					}
					fileWriter.close();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, " \nError writing to CSV file.\n"+ e.getMessage());
				}
			}
		}
	}
	public TableExporter(JTable table, boolean copyToClipboard) {
     StringBuffer sbf=new StringBuffer();

			TableColumnModel cm = table.getColumnModel();
			for (int i=0;i<cm.getColumnCount(); i++) {
				String s = ""+cm.getColumn(i).getHeaderValue();
        s = s.replace('\n',' ');
        s = s.trim();
        if (i>0)
        	s = "\t" + s;
        sbf.append(s);
			}
			sbf.append("\n");
		
			for (int i = 0; i < table.getRowCount(); ++i) {
				for (int j = 0; j < table.getColumnCount(); ++j) {
				  String s;
				  if (table.getValueAt(i,j) != null)
					  s = table.getValueAt(i, j).toString();
          else
					  s = "";
						
					// clean up the text string to remove $, %, and commas
          s = s.replaceAll("\\$", "");
          s = s.replaceAll("%", "");
          s = s.replaceAll(", ", "; ");
          s = s.replaceAll(",", "");
          if (j > 0)
            	s = "\t" + s;
          sbf.append(s);
				}
				sbf.append("\n");
			}

		  StringSelection stsel  = new StringSelection(sbf.toString());
      Clipboard system = Toolkit.getDefaultToolkit().getSystemClipboard();
      system.setContents(stsel,stsel);	
	}

}
